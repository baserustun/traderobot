#The actual query
import json
from urllib.request import Request, urlopen
import urllib
import codecs
import threading
from threading import Thread
import time
from lxml import html
import requests

class getData(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
    def run(self):
        counter = 0
        timestr = time.strftime("%Y%m%d-%H%M%S")
        fData = open(timestr + '.csv',"w+")

        while True:

            try:
                req = Request('https://www.btcturk.com/api/ticker', headers={'User-Agent': 'Mozilla/5.0'})
                webpage = urlopen(req).read()
            except urllib.error.HTTPError as e:
                # Possible issue with CloudFlare, just fall through
                if e.code == 525:
                    print("525 error")
                    continue
                if e.code == 524:
                    print("524 error")
                    continue
                if e.code == 504:
                    print("504 error")
                    continue
                if e.code == 502:
                    print("502 error")
                    continue
                if e.code == 403:
                    print("403 error")
                    continue
                if e.code == 500:
                    print("500 error")
                    continue
                if e.code == 409:
                    print("409 error")
                    continue
                
            except urllib.error.URLError as e:
                print("URL error occured1")
                continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
            page = requests.get('https://www.marketwatch.com/investing/currency/usdtry')
            tree = html.fromstring(page.content)
            usdPrice = tree.xpath('//span[@class="value"]/text()')
            print(usdPrice)
            objBTCTurk = webpage.decode('utf8').replace("'", '"')
            data = json.loads(objBTCTurk)
            objBTCTurkFilt = [x for x in data if x['pair'] == 'BTCTRY' or x['pair'] == 'BTCUSDT' or x['pair'] == 'ETHTRY' or x['pair'] == 'XRPTRY' or x['pair'] == 'USDTTRY' ]
            fData.write(json.dumps(objBTCTurkFilt))
            fData.write("\n")   
            
            try:
                req = Request('https://cex.io/api/tickers/USD', headers={'User-Agent': 'Mozilla/5.0'})
                webpage = urlopen(req).read()
            except urllib.error.HTTPError as e:
                # Possible issue with CloudFlare, just fall through
                if e.code == 525:
                    print("525 error")
                    continue
                if e.code == 524:
                    print("524 error")
                    continue
                if e.code == 504:
                    print("504 error")
                    continue
                if e.code == 502:
                    print("502 error")
                    continue
                if e.code == 403:
                    print("403 error")
                    continue
                if e.code == 500:
                    print("500 error")
                    continue
                if e.code == 409:
                    print("409 error")
                    continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                
            except urllib.error.URLError as e:
                print("URL error occured2")
                continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                
            objCexio = webpage.decode('utf8').replace("'", '"')
            data = json.loads(objCexio)
            #objCexioFilt = [x for x in data if x['pair'] == 'BTC:USD' or x['pair'] == 'ETH:USD' ]
            fData.write(json.dumps(data))
            fData.write("\n")

            try:    
                req = Request('https://koineks.com/ticker', headers={'User-Agent': 'Mozilla/5.0'})
                webpage = urlopen(req).read()
            except urllib.error.HTTPError as e:
                # Possible issue with CloudFlare, just fall through
                if e.code == 525:
                    print("525 error")
                    continue
                if e.code == 524:
                    print("524 error")
                    continue
                if e.code == 504:
                    print("504 error")
                    continue
                if e.code == 502:
                    print("502 error")
                    continue
                if e.code == 403:
                    print("403 error")
                    continue
                if e.code == 500:
                    print("500 error")
                    continue
                if e.code == 409:
                    print("409 error")
                    continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                raise
            except urllib.error.URLError as e:
                print("URL error occured3")
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                continue
                
            objCoinex = webpage.decode('utf8').replace("'", '"')
            data = json.loads(objCoinex)
            #objCoinexFilt = [x for x in data if x['name'] == 'Bitcoin' or x['name'] == 'Etherium' or x['name'] == 'USDTether' or x['name'] == 'XRP' or x['name'] == 'Litecoin' ]
            fData.write(json.dumps(data))
            fData.write("\n")

            try:
                req = Request('https://www.binance.com/api/v3/ticker/bookTicker', headers={'User-Agent': 'Mozilla/5.0'})
                webpage = urlopen(req).read()
            except urllib.error.HTTPError as e:
                # Possible issue with CloudFlare, just fall through
                if e.code == 525:
                    print("525 error")
                    continue
                if e.code == 524:
                    print("524 error")
                    continue
                if e.code == 504:
                    print("504 error")
                    continue
                if e.code == 502:
                    print("502 error")
                    continue
                if e.code == 403:
                    print("403 error")
                    continue
                if e.code == 500:
                    print("500 error")
                    continue
                if e.code == 409:
                    print("409 error")
                    continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                
            except urllib.error.URLError as e:
                print("URL error occured4")
                continue
                # TODO: ... handle all the other 5xx errors
                # Raise the original exception
                
            objBinance = webpage.decode('utf8').replace("'", '"')
            data = json.loads(objBinance)
            objBinanceFilt = [x for x in data if x['symbol'] == 'BTCUSDT' or x['symbol'] == 'ETHUSDT' ]
            fData.write(json.dumps(objBinanceFilt))
            fData.write("\n")

            #print(json.dumps(objBTCTurkFilt))
            #print(json.dumps(objBinanceFilt))
            #print(json.dumps(objCoinex))
            #print(json.dumps(objCexio))

            time.sleep(1)

            counter = counter + 1
            if counter == 28000:
                timestrng = time.strftime("%Y%m%d-%H%M%S")
                fData = open(timestrng + '.csv',"w+")
                counter = 0
            else:
                print(counter)
getData()

while True:
    pass
